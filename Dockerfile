# Stage 1: Build the Rust application
FROM rust:latest as builder

# Create application directory
WORKDIR /usr/src/app

# Copy Rust project files
COPY ./Cargo.toml ./Cargo.lock ./

# Prepare for dependencies compilation
RUN mkdir src/ && \
    echo "fn main() {println!(\"if you see this, the build broke\")}" > src/main.rs && \
    cargo build --release && \
    rm -f target/release/deps/final_group16*

# Copy the actual source code files into the image
COPY ./src ./src

# Build the application for release
RUN cargo build --release

# Stage 2: Setup the runtime environment
FROM python:latest

# Set working directory
WORKDIR /app

# Install system dependencies
RUN apt-get update && apt-get install -y --no-install-recommends \
    build-essential \
    libffi-dev \
    libssl-dev \
    curl \
    && rm -rf /var/lib/apt/lists/*

# Install Rust (required for compiling dependencies in Python packages)
RUN curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y
ENV PATH="/root/.cargo/bin:${PATH}"

# Install PyTorch and transformers
RUN pip install --no-cache-dir torch transformers

# Copy the compiled Rust binary and the Python script
COPY --from=builder /usr/src/app/target/release/final_group16 .
COPY src/translation_service.py /usr/local/bin/
RUN chmod +x /usr/local/bin/translation_service.py

# Expose the port the app will run on
EXPOSE 8080

# Command to run the application
CMD ["./final_group16"]
