import sys
import json
from transformers import pipeline

class Translator:
    def __init__(self):
        self.translator = pipeline("translation_en_to_zh", model="Helsinki-NLP/opus-mt-en-zh")

    def translate_text(self, text):
        translation = self.translator(text, max_length=40)
        return translation[0]['translation_text']

def main():
    try:
        translator = Translator()
        input_text = sys.argv[1]
        result = translator.translate_text(input_text)
        print(json.dumps({"response": result}))
    except Exception as e:
        print(json.dumps({"error": "Translation failed", "details": str(e)}))

if __name__ == "__main__":
    main()

