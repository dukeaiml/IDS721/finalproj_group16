use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use std::process::Command;
use serde_json::{Value, json};

async fn translate_request(prompt: web::Json<Value>) -> impl Responder {
    let input_value = prompt.into_inner();
    let input_text = input_value.get("text").and_then(Value::as_str).unwrap_or("");

    let process = Command::new("python3")
        .arg("./src/translation_service.py")
        .arg(input_text)
        .output();

    match process {
        Ok(output) => {
            if !output.stderr.is_empty() {
                eprintln!("Python STDERR: {}", String::from_utf8_lossy(&output.stderr));
            }

            let output_str = String::from_utf8_lossy(&output.stdout);
            println!("Python output: {}", output_str); // Print the Python output

            match serde_json::from_str::<Value>(&output_str) {
                Ok(json) => HttpResponse::Ok().json(json),
                Err(e) => HttpResponse::BadRequest().json(json!({"error": "Bad JSON", "details": e.to_string()}))
            }
        },
        Err(e) => HttpResponse::InternalServerError().json(json!({"error": "Command failed", "details": e.to_string()}))
    }
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/translate", web::post().to(translate_request))
    })
    .bind("127.0.0.1:8080")?
    .run()
    .await
}


