# IDS721 Final Project -- Language Translation using Rust web service

This project aims to provide language translation capabilities using Rust and Python. It includes steps for local testing, local Docker testing, deployment on AWS, and setting up a CI/CD pipeline. 

The model `Helsinki-NLP/opus-mt-en-zh` selected is designed to handle text translation requests and provides a RESTful API for communication. It takes input text in one language and translates it into another language using advanced machine learning techniques.

To use the translation service, make a POST request to the API endpoint with the input text in the request body. The service will then process the request and return the translated text as the response.

## Demo Video

Youtube Link: https://youtu.be/LLh4oimzSQc

## Getting Started

To get started with the project, follow the instructions below.

### Local Testing

1. Create the project: 

`cargo new <project-name>`


2. Navigate to the project directory: 

`cd <project_directory>`


3. Add the required dependencies in `cargo.toml`.

4. Run the Rust web service: `cargo run`

5. Test the translation functionality using cURL: 
```
curl -X POST -H "Content-Type: application/json" -d '{"text": "Hello, world!"}' http://127.0.0.1:8080/translate
```

#### Local Curl Screenshot
![local_test](imgs/local_curl.png)

### Local Containerization in DockerHub

1. Install Docker on your local machine and create dockerfile.


2. Build the Docker image: 
```
docker build -t <image-name> .
```


3. Run the Docker container: 
```
docker run -p 8080:8080 <image-name>
```



### Deployment on AWS

1. Register an Elastic Container Registry (ECR) repository.


2. Build and push the Docker image to the ECR repository.

ECR Push Command Line:
![ecr](imgs/ecr_push.png)


Image in ECR:
![ecr_image](imgs/images.png)


3. Create an Amazon Elastic Kubernetes Service (EKS) cluster.
![eks](imgs/eks.png)

4. Set up an EC2 instance and load balancer for the translation service using configured files.

```bash
kubectl apply -f EKS/eks-sample-deployment.yaml
kubectl apply -f EKS/eks-sample-service.yaml  
```

EC2 Screenshot:
![ec2](imgs/ec2.png)


5. Configure monitoring and metrics in loadbalancer for the service to evaluate its health and performance.
![monitor](imgs/monitor.png)

### Setting up CI/CD Pipeline

1. Set up a CI/CD pipeline.
2. Configure the pipeline to automatically build, test,and deploy the translation service on AWS.
3. Ensure the pipeline supports continuous development and deployment.

![pipeline](imgs/ci-pipeline.png)


## Result
Attached is result from POSTMAN web service using AWS EKS API Endpoint
![api](imgs/aws_curl.png)

## Documentation
For detailed setup, deployment, and usage instructions, please refer to the following AWS official documentation:

- [IAM Documentation](https://docs.aws.amazon.com/iam/)
- [EKS Documentation](https://docs.aws.amazon.com/eks/)
- [ECR Documentation](https://docs.aws.amazon.com/ecr/)
- [EC2 Documentation](https://docs.aws.amazon.com/ec2/)
- [Load Balancers Documentation](https://docs.aws.amazon.com/elasticloadbalancing/)


